using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            StartCoroutine(DelayedPlayerSpawn());
        }
    }

    IEnumerator DelayedPlayerSpawn()
    {
        yield return new WaitForSeconds(3);
        PhotonNetwork.Instantiate(playerPrefab.name, SpawnPointManager.instance.getSpawnPoint(), Quaternion.identity);
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
