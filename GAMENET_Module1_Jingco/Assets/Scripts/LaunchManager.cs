using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun; // Import
using Photon.Realtime; // Handles All Rooms and Lobbies

public class LaunchManager : MonoBehaviourPunCallbacks // Contains several override methods to determine the status of our connection
{
    // 1ST 
    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;


    // 9TH
    void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true; // Every Player that Joins In will Load the Scene that our Master Client has Loaded
    }

    // Start is called before the first frame update
    void Start()
    {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 4TH
    public override void OnConnectedToMaster() // Determine if Poject is Connected to Photon
    {
        // base.OnConnectedToMaster();
        Debug.Log(PhotonNetwork.NickName + " connected to photon servers");
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
    }

    // 3RD
    public override void OnConnected() // Called once Photon Detects we are Connected to the Internet
    {
        Debug.Log("connected to internet");
    }

    // 6th
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);
        CreateAndJoinRoom(); 
    }

    // 2ND
    public void ConnectToPhotonServer()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings(); // Connect to Photon Servers
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }

    }

    // 5th
    public void JoinRandomRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    // 7th
    private void CreateAndJoinRoom()
    {
        string randomRoomName = "Room " + Random.Range(0, 10000);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
    }

    // 8th
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + " has entered room " + PhotonNetwork.CurrentRoom.Name + ". Room has now " + PhotonNetwork.CurrentRoom.PlayerCount + " players");
    }
}
